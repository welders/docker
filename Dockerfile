# use python 3.5
FROM python:3.5

# install prerequisites
RUN apt-get update
RUN pip install pipenv

# set workdir
WORKDIR /app

# clone the Dockerfile
RUN git clone https://radujica@bitbucket.org/welders/soaprest.git 

# install; don't install in virtualenv but in system, 
# since this container already has a 'virtual' python from above
RUN cd /app/soaprest/1.2 && pipenv install --system

# make our port available
EXPOSE 9998

# define env var
ENV NAME Shortener

# run Shortener; no pipenv run because the packages are in the system already
CMD ["python", "/app/soaprest/1.2/shortener_service.py"]
